# Basthon-Kernel

This is the kernel used in [Basthon-Console](https://console.basthon.fr) and [Basthon-Notebook](https://notebook.basthon.fr), two client-side Python learning platforms running in the browser. **Basthon-Kernel** is *not* intended to be used as is. It is the interface between the JavaScript Python interpret (Pyodide) and **Basthon-Console** and **Basthon-Notebook**.

Entry point of the project is the [Basthon website](https://basthon.fr). For more information, see the [Basthon's readme](https://framagit.org/casatir/basthon-kernel/-/blob/master/README.md#basthon).


## Intallation

    $ pip install basthon-kernel

Then in chosen folder:

    $ python -m basthon-kernel --install


## Sources

**Basthon** sources are hosted on [Framagit](https://framagit.org/):

 - [Basthon-Console](https://framagit.org/casatir/basthon-console)
 - [Basthon-Notebook](https://framagit.org/casatir/basthon-notebook)
