from setuptools import setup, find_packages

from pathlib import Path
import shutil
import sys

this_dir = Path.cwd()
root_dir = this_dir.parent
src_dest = ((root_dir / "src" / "basthon-js", '.'),
            (root_dir / "src" / "basthon-py" / "build", 'basthon-py'),
            (root_dir / "src" / "modules", 'modules'))

with open("README.md") as f:
    LONG_DESCRIPTION = f.read()

command = sys.argv[1]

if command == "sdist":
    # before creating the distribution, copy files from other locations in
    # the repository
    print("copying files...")
    basthon_dir = this_dir / "basthon-kernel"
    data_dir = basthon_dir / "data"

    # copy files from basthon into data_dir
    content = []

    for src, dest in src_dest:
        for path in src.rglob("*"):
            exts = ('.js', '.map', '.data') if path.parent == src else ('.whl',)
            if path.is_file() and path.suffix in exts:
                dst = data_dir / dest / path.name
                dst.parent.mkdir(exist_ok=True)
                shutil.copyfile(path, dst)

setup(
    name='basthon-kernel',

    version='0.15.4',
    description=('Basthon-kernel is used in Basthon Notebook'
                 ' and Basthon Console projects to provide '
                 'Python interpreter in the browser.'),

    long_description=LONG_DESCRIPTION,

    long_description_content_type="text/markdown",

    # The project's main homepage.
    url='https://framagit.org/casatir/basthon-kernel/',

    # Author details
    author='Romain Casati',
    author_email='romain.casati@ac-orleans-tours.fr',

    packages=find_packages(),

    # Choose your license
    license='License :: OSI Approved :: GNU General Public License v3 (GPLv3)',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Interpreters',

        'Operating System :: OS Independent',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3',
    ],

    # What does your project relate to?
    keywords='Python browser',

    package_data={
        'basthon-kernel': ['data/*.*', 'data/*/*.*']
    },


)
