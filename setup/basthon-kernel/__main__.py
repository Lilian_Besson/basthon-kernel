"""This script creates the basic structure of a Basthon kernel project in the
current directory.
"""

from pathlib import Path
import shutil
import argparse

from . import implementation


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--install',
                        help='Install Basthon kernel in current directory',
                        action="store_true")

    args = parser.parse_args()

    if args.install:
        print(f'Installing Basthon kernel {implementation} in current directory')

        data_path = Path(__file__).parent / 'data'

        for path in data_path.rglob("*"):
            if path.is_file():
                shutil.copyfile(path, path.relative_to(data_path))
            elif path.is_dir():
                path.relative_to(data_path).mkdir(exist_ok=True)


if __name__ == "__main__":
    main()
