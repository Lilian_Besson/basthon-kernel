examples = {
    "courbes.py": {
        'desc': "Tracé de courbes avec Matplotlib",
    },
    "project-euler.py": {
        'desc': "Un problème du <a href='https://projecteuler.net/'>Projet Euler</a>",
    },
    "spirale.py": {
        'desc': "Une spirale avec Turtle",
    },
    "3d-plot.ipynb": {
        'desc': "Tracé de surface avec Matplotlib",
    },
    "sklearn.ipynb": {
        'desc': "Classification avec Scikit-Learn",
    },
    "sympy.ipynb": {
        'desc': "Calcul formel avec Sympy",
    },
    "decorateurs.ipynb": {
        'desc': "Fractales, Turtle et décorateurs",
    },
    "pypi.ipynb": {
        'desc': "Chargement d'un module PyPi",
    },
    "easter-eggs.ipynb": {
        'desc': "Quelques easter eggs de (C)Python",
    },
    "markdown.ipynb": {
        'desc': "Utilisation de Markdown dans un notebook",
    },
    "folium.py": {
        'desc': "Des cartes avec Folium",
    },
    "folium-notebook.ipynb": {
        'desc': "Folium dans un notebook",
    },
    "linear-regression.py": {
        'desc': "Régression linéaire",
    },
    "lorenz.py": {
        'desc': "Attracteurs de Lorenz",
    },
    "requests.py": {
        'desc': "Requêtes HTTP avec requests",
    },
    "dom.ipynb": {
        'desc': "Modification du DOM",
    },
    "pandas.ipynb": {
        'desc': "Utilisation de Pandas",
    },
    "osm-process.ipynb": {
        'desc': "Récupération et traitement des données OpenStreetMap de la France",
    },
    "contour-france.py": {
        'desc': "Contour de la France continentale (OSM)",
    },
    "centre-france.ipynb": {
        'desc': "Où est le centre de la France ?",
    },
    "turtle-writing.ipynb": {
        'desc': "Une tortue qui sait écrire",
    },
    "lists.ipynb": {
        'desc': "Des listes chaînées bien typées",
    },
    "graphviz-console.py": {
        'desc': "Graphviz dans la console",
    },
    "graphviz-notebook.ipynb": {
        'desc': "Graphviz dans un notebook",
    },
    "traitement-image.ipynb": {
        'desc': "Traitement d'image avec Numpy et Matplotlib",
        'aux': "examples/sandbox.png",
    },
    "snake.ipynb": {
        'desc': "Traitement d'image avancé avec Scipy",
        'aux': "examples/snake.png"
    },
    "p5-square.py": {
        'desc': "Un exemple simple d'utilisation de p5",
    },
    "p5-basthon-on-fire.py": {
        'desc': "p5 enflamme Basthon",
    },
    "p5-torus.py": {
        'desc': "Animation 3d avec p5",
    },
    "p5-tree.py": {
        'desc': "Un arbre récursif avec p5",
    },
    "p5-waves.py": {
        'desc': "De drôles de bestioles avec p5",
    },
    "p5-camera.py": {
        'desc': "Utilisation de la webcam avec p5",
    },
    "p5.ipynb": {
        'desc': "Utilisation de p5 dans un notebook",
    },
    "p5-balls.ipynb": {
        'desc': "Des balles qui rebondissent avec p5",
    },
    "p5-instance.ipynb": {
        'desc': "Plusieurs dessins avec p5 en mode instance",
    },
    "slideshow.ipynb": {
        'desc': "Un diaporama avec un notebook (RISE) "
    },
    "moon.ipynb": {
        'desc': "Manipulation d'images avec PIL",
        'aux': "examples/moon.jpg",
    },
}
