import js
import sys
from . import _pyodide_console, kernel, _pyodide_base, packages

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ["InteractiveConsole"]


class InteractiveConsole(_pyodide_console.InteractiveConsole):
    """ This is the Python's part of Basthon kernel """
    def __init__(self, *args, **kwargs):
        self.execution_count = None
        super().__init__(*args, **kwargs)
        self.runner = _pyodide_base.CodeRunner(globals=self.locals,
                                               filename="<input>")
        self.start()

    def roll_in_history(self, code):
        """ Manage storing in 'In' ala IPython. """
        self.locals['In'].append(code)

    def roll_out_history(self, out):
        """ Manage storing in 'Out', _, __, ___ ala IPython. """
        outputs = self.locals['Out']
        # out is not always stored
        if out is not None and out is not outputs:
            outputs[self.execution_count] = out
            self.locals['___'] = self.locals['__']
            self.locals['__'] = self.locals['_']
            self.locals['_'] = out

    def start(self):
        """
        Start the Basthon kernel and fill the namespace.
        """
        self.execution_count = 0
        self.locals.clear()
        self.locals.update({
            '__name__': '__console__',
            '__doc__': None,
            '_': '',
            '__': '',
            '___': '',
            'In': [''],
            'Out': {},
        })

    def stop(self):
        """
        Stop the Basthon kernel.
        """
        pass

    def restart(self):
        """
        Restart the Basthon kernel.
        """
        self.stop()
        self.start()

    def more(self, source):
        """ Is the source ready to be evaluated or want we more?

        Usefull to set ps1/ps2 for teminal prompt.
        """
        try:
            code = self.compile(source, self.filename, "exec")
        except (OverflowError, SyntaxError, ValueError):
            return False

        if code is None:
            return True

        return False

    def showtraceback(self):
        """ Remove one more traceback level. """
        ei = sys.exc_info()
        sys_exc_info = sys.exc_info
        sys.exc_info = lambda: (ei[0], ei[1], ei[2].tb_next)
        super().showtraceback()
        sys.exc_info = sys_exc_info

    def find_imports_with_errors(self, source):
        """ TODO. """
        try:
            return packages.find_imports(source)
        except (OverflowError, SyntaxError, ValueError):
            self.showsyntaxerror(self.runner.filename)

    def run_script(self, source):
        """ TODO. """
        self.execution_count += 1
        exec_count = self.execution_count
        self.roll_in_history(source)

        with self.stdstreams_redirections():
            pkgs = self.find_imports_with_errors(source)

        if pkgs is None:
            return js.Promise.resolve((None, exec_count))

        def run(*args):
            result = None

            with self.stdstreams_redirections():
                try:
                    result = self.runner.run(source)
                except SystemExit:
                    raise
                except Exception:
                    self.showtraceback()

            self.roll_out_history(result)
            if result is not None:
                result = kernel.format_repr(result)
            return result, exec_count

        return packages.load_and_hack(pkgs).then(run)

    def eval(self, code, stdout_callback, stderr_callback, data=None):
        """
        Evaluation of Python code with communication managment
        with the JS part of Basthon and stdout/stderr catching.
        data can be accessed in code through '__eval_data__' variable
        in global namespace.

        Results:
        --------
        A promise that resolves with the formated result.
        """
        self.locals['__eval_data__'] = data
        self.stdout_callback = stdout_callback
        self.stderr_callback = stderr_callback

        def callback(*args):
            return self.run_script(code)

        self.run_complete = self.run_complete.then(callback)
        return self.run_complete
