"""
Package (or module) loader.

It manage Pyodide and Basthon's modules load.
"""

import pyodide
import js
from . import _hack_modules


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ["find_imports", "load_and_hack"]


# Link to useful pyodide's dict
_import_name_to_package_name = js.pyodide._module.packages.import_name_to_package_name
_import_name_to_package_name = {k: _import_name_to_package_name[k]
                                for k in dir(_import_name_to_package_name)
                                if isinstance(_import_name_to_package_name[k], str) and '.' not in k}

# Available packages in Pyodide.
_pyodide_pkgs = set(_import_name_to_package_name.keys())

# Root path to internal modules
_internal_modules_path = js.Basthon.basthonModulesRoot

# Packages not implemented in Pyodide but in Basthon (internal)
# (dict pointing to Pypi or internal addresse).
_internal_pkgs = {
    "turtle": {
        "path": f"{_internal_modules_path}/turtle-0.0.1-py3-none-any.whl",
    },
    "requests": {
        "path": f"{_internal_modules_path}/requests-0.0.1-py3-none-any.whl",
    },
    "proj4py": {
        "path": f"{_internal_modules_path}/proj4py-0.0.1-py3-none-any.whl",
        "deps": ["pkg_resources"],
    },
    "folium": {
        "path": "folium",  # loaded from PyPi
    },
    "graphviz": {
        "path": f"{_internal_modules_path}/graphviz-0.0.1-py3-none-any.whl",
        "deps": ["pkg_resources"],
    },
    "IPython": {
        "path": f"{_internal_modules_path}/IPython-0.0.1-py3-none-any.whl",
    },
    "p5": {
        "path": f"{_internal_modules_path}/p5-0.0.1-py3-none-any.whl",
        "deps": ["pkg_resources"],
    },
}

# Union of internal and pyodide packages.
_all_pkgs = _pyodide_pkgs | set(_internal_pkgs.keys())

# Packages already loaded.
_loaded = set()


def _load_pyodide(packages):
    """ Load Pyodide packages (async). """
    if isinstance(packages, str):
        packages = [packages]

    if not packages:
        return js.Promise.resolve()
    # from Python name to Pyodide name
    return js.pyodide.loadPackage([_import_name_to_package_name[p]
                                   for p in packages])


def _load_internal(packages):
    """ Load internal module with micropip (async). """
    if isinstance(packages, str):
        packages = [packages]

    if not packages:
        return js.Promise.resolve()

    def micropip_wrapper(*args):
        return micropip_install([_internal_pkgs[p]['path']
                                 for p in packages])

    return _load_pyodide('micropip').then(micropip_wrapper)


def _internal_dependencies(packages):
    """ Return the set of dependencies of a list of internal packages. """
    def dependencies(package):
        deps = set(_internal_pkgs.get(package, {}).get("deps", []))
        return deps.union(*(dependencies(p) for p in deps))

    packages = set(packages)
    return set.union(*(dependencies(p) for p in packages)) - packages


def load_and_hack(packages):
    """ Load and hack modules (internal or Pyodide). """
    if isinstance(packages, str):
        packages = [packages]

    # remove already loaded
    packages = set(packages) - _loaded

    if not packages:
        return js.Promise.resolve()

    packages = packages | _internal_dependencies(packages)
    pyodide_packages = packages & _pyodide_pkgs
    internal_packages = packages & set(_internal_pkgs.keys())

    def callback(*args):
        if packages:
            _loaded.update(packages)
            _hack_modules.hack(packages)

    return js.Promise.all((_load_pyodide(pyodide_packages),
                           _load_internal(internal_packages))).then(callback)


def find_imports(code):
    """
    Wrapper around pyodide.find_imports.
    """
    if not isinstance(code, str):
        try:
            code = code.tobytes()
        except Exception:
            pass
        code = code.decode()
    return pyodide.find_imports(code)


def micropip_install(packages):
    """ Load packages using micropip and return a promise. """
    import micropip
    return micropip.install(packages)
