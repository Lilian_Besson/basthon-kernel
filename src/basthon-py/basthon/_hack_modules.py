from . import kernel
from js import document


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


def hack_matplotlib():
    """
    Hack the Wasm backend of matplotlib to render figures.
    """
    from matplotlib.backends.wasm_backend import FigureCanvasWasm as wasm_backend

    # hacking root node creation
    def create_root_element(self):
        self.root = document.createElement("div")
        return self.root

    wasm_backend.create_root_element = create_root_element

    # hacking show
    if not hasattr(wasm_backend, "_original_show"):
        wasm_backend._original_show = wasm_backend.show

    def show(self):
        res = self._original_show()
        kernel.display_event({"display_type": "matplotlib",
                              "content": self.root})
        return res

    show.__doc__ = wasm_backend._original_show.__doc__
    wasm_backend.show = show


def hack_turtle():
    """
    Hack Turtle to render figures.
    """
    from turtle import Screen

    # hacking show_scene
    if not hasattr(Screen, "_original_show_scene"):
        Screen._original_show_scene = Screen.show_scene

    def show_scene(self):
        root = self._original_show_scene()
        kernel.display_event({"display_type": "turtle",
                              "content": root})
        self.restart()

    show_scene.__doc__ = Screen._original_show_scene.__doc__

    Screen.show_scene = show_scene


def hack_sympy():
    """
    Hack Sympy to render expression using LaTeX (and probably MathJax).
    """
    import sympy

    def pretty_print(*args, sep=' '):
        """
        Print arguments in latex form.
        """
        latex = sep.join(sympy.latex(expr) for expr in args)
        kernel.display_event({"display_type": "sympy",
                              "content": f"$${latex}$$"})

    sympy.pretty_print = pretty_print


def hack_folium():
    """
    Hack Folium to render maps.
    """
    from folium import Map

    def display(self):
        """
        Render map to html.
        """
        kernel.display_event({"display_type": "html",
                              "content": self._repr_html_()})

    Map.display = display


def hack_pandas():
    """
    Hack Pandas to render data frames.
    """
    from pandas import DataFrame

    def display(self):
        """
        Render data frame to html.
        """
        kernel.display_event({"display_type": "html",
                              "content": self._repr_html_()})

    DataFrame.display = display


def hack_PIL():
    from PIL import Image, ImageShow
    import io
    from base64 import b64encode

    # pluging for Notebook
    def _repr_png_(self):
        byio = io.BytesIO()
        self.save(byio, format='PNG')
        return b64encode(byio.getvalue()).decode()

    Image.Image._repr_png_ = _repr_png_

    # pluging image.show()
    class basthonviewer(ImageShow.Viewer):
        def show_image(self, image, **options):
            kernel.display(image)

    ImageShow._viewers = []
    ImageShow.register(basthonviewer)


def hack(modules):
    """ Hack a list of modules. """
    if isinstance(modules, str):
        modules = (modules,)

    for module in modules:
        hack_func = globals().get(f"hack_{module}")
        if callable(hack_func):
            hack_func()
