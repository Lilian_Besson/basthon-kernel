(function (root, factory) {
    // requirejs?
    if (typeof define === 'function') {
        define(['module'], factory);
    // TypeScript?
    } else if (typeof exports === 'object') {
        module.exports = factory();
    // default goes to global namespace
    } else {
        root.Basthon = factory();
    }
}(this, function (module) {
    'use strict';

    let that = {};

    /**
     * Where to find pyodide.js (private).
     */
    that._pyodideUrl = "https://cdn.jsdelivr.net/pyodide/v0.16.1/full/pyodide.js";

    /**
     * Dirname remove basename/filename from url.
     */
    that.dirname = function (url) {
        return url.substring(0, url.lastIndexOf("/"));
    };

    /**
     * Is Basthon loaded ?
     */
    that.loaded = false;

    /**
     * Get the URL of the current script (usefull for serving basthon-py)
     */
    that.urlScript = (function () {
        if( module != null ) {
            let url = window.location.origin + window.location.pathname;
            url = url.substring(0, url.lastIndexOf('/')) + "/";
            return url + module.uri;
        } else
            return document.currentScript.src;
    })();

    /**
     * Get the URL of Basthon kernel root dir.
     */
    that.basthonRoot = that.dirname(that.urlScript);

    /**
     * Get the URL of Basthon modules dir.
     */
    that.basthonModulesRoot = that.basthonRoot + "/modules";
    
    /**
     * Downloading data (bytes array) as filename (opening browser dialog).
     */
    that.download = function (data, filename) {
        let blob = new Blob([data], { type: "application/octet-stream" });
        let anchor = document.createElement("a");
        anchor.download = filename;
        anchor.href = window.URL.createObjectURL(blob);
        anchor.target ="_blank";
        anchor.style.display = "none"; // just to be safe!
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);
    };
    
    /**
     * Dynamically load a script asynchronously.
     */
    that.loadScript = function (url) {
        return new Promise(function(resolve, reject) {
            let script = document.createElement('script');
            script.onload = resolve;
            script.onerror = reject;
            script.src = url;
            document.head.appendChild(script);
        });
    };

    /**
     * A promise that resolve once the page is loaded.
     */
    that.pageLoad = (function () {
        return new Promise(function (resolve, reject) {
            if( document.readyState === 'complete' ) {
                resolve();
            } else {
                window.addEventListener("load", function() {
                    // just to be safe
                    window.removeEventListener("load", this);
                    resolve();
                });
            }
        });
    })();

    /**
     * What to do when loaded (private).
     */
    that._onload = function() {
        that.loaded = true;
        // connecting eval to basthon.eval.request event.
        that.addEventListener("eval.request", that.evalFromEvent);
        // get the version of Python from Python
        that.pythonVersion = pyodide.runPython("import platform ; platform.python_version()");
        // this is for avoiding "Unknown package 'basthon'" error
        // but can be removed  with 0.17.0 since it is fixed upstream
        const consoleErrorBck = console.error;
        console.error = () => {};
        return pyodide.loadPackage(that.basthonRoot + "/basthon-py/basthon.js").then(() => {
            pyodide.runPythonAsync("import basthon");
            console.error = consoleErrorBck;
        });
    };

    /**
     * Wrapper around XHR through a promise.
     */
    that.xhr = function (params) {
        const xhr = new XMLHttpRequest();
        xhr.open(params.method, params.url, true);
        xhr.responseType = params.responseType;
        const promise = new Promise(function(resolve, reject) {
            xhr.onload = function() {
                if( xhr.status >= 200 && xhr.status < 300 ) {
                    resolve(xhr.response);
                } else {
                    reject(xhr);
                }
            };
            xhr.onerror = function () { reject(xhr); };
        });
        // headers
        if (params.headers) {
            Object.keys(params.headers).forEach(function (key) {
                xhr.setRequestHeader(key, params.headers[key]);
            });
        }
        // data
        let data = params.data;
        if (data && typeof data === 'object') {
            data = JSON.stringify(data);
        }
        xhr.send(data);
        return promise;
    };

    /**
     * Start the Basthon kernel asynchronously.
     */
    that.load = (async function () {
        /* testing if Pyodide is installed locally */
        try {
            const url = that.basthonRoot + "/pyodide/pyodide.js";
            await that.xhr({method: "HEAD", url: url});
            that._pyodideUrl = url;
        } catch (e) {}

        // forcing Pyodide to look at the right location for other files
        window.languagePluginUrl = that._pyodideUrl.substr(0, that._pyodideUrl.lastIndexOf('/')) + '/';
        
        // avoid conflict with requirejs and use it when available.
        try {
            if( typeof requirejs !== 'undefined' ) {
                requirejs.config({paths: {pyodide: that._pyodideUrl.slice(0, -3)}});
                await new Promise(function (resolve, reject) {
                    require(['pyodide'], resolve, reject);
                });
            } else {
                await that.loadScript(that._pyodideUrl);
            }
        } catch (error) {
            console.log(error);
            console.error("Can't load pyodide.js");
        }

        await languagePluginLoader.then(
            that._onload,
            function() { console.error("Can't load Python from Pyodide"); });
        
        // waiting until page is loaded
        await that.pageLoad;
    })();
    
    /**
     *  Ease the creation of events.
     */
    that.dispatchEvent = function (eventName, data) {
        const event = new CustomEvent("basthon." + eventName, { detail: data });
        document.dispatchEvent(event);
    };

    /**
     * Ease the event processing.
     */
    that.addEventListener = function (eventName, callback) {
        document.addEventListener(
            "basthon." + eventName,
            function (event) { callback(event.detail); });
    };

    /**
     * Cloning function.
     */
    that.clone = function (obj) {
        // simple trick that is enough for our purpose.
        return JSON.parse(JSON.stringify(obj));
    };

    /**
     * Basthon async code evaluation function.
     */
    that.evalAsync = async function (code, outCallback, errCallback,
                                     loadPackageCallback, data=null) {
        if( !that.loaded ) { return ; }
        if( typeof outCallback === 'undefined' ) {
            outCallback = function (text) { console.log(text); };
        }
        if( typeof errCallback === 'undefined' ) {
            errCallback = function (text) { console.error(text); };
        }
        // loading dependencies are loaded by eval
        return await pyodide.globals.basthon.kernel.eval(code, outCallback, errCallback, data);
    };

    /**
     * Basthon evaluation function callback.
     * It is not used directly but through basthon.eval.request event.
     * A Python error throw basthon.eval.error event.
     * Output on stdout/stderr throw basthon.eval.output.
     * Matplotlib display throw basthon.eval.display event.
     * When computation is finished, basthon.eval.finished is thrown.
     */
    that.evalFromEvent = function (data) {
        if( !that.loaded ) { return ; }

        let stdCallback = function (std) { return function (text) {
            let dataEvent = that.clone(data);
            dataEvent.stream = std;
            dataEvent.content = text;
            that.dispatchEvent("eval.output", dataEvent);
        }};
        let outCallback = stdCallback("stdout");
        let errCallback = stdCallback("stderr");

        return that.evalAsync(data.code, outCallback, errCallback,
                              that.hackPackagesCallback, data)
            .then(function (args) {
                const result = args[0];
                const executionCount = args[1];
                let dataEvent = that.clone(data);
                dataEvent.execution_count = executionCount;
                if( typeof result !== 'undefined' ) {
                    dataEvent.result = result;
                }
                that.dispatchEvent("eval.finished", dataEvent);
            }, function (error) {
                errCallback(error.toString());
                let dataEvent = that.clone(data);
                dataEvent.error = error;
                dataEvent.execution_count = pyodide.globals.basthon.kernel.execution_count();
                that.dispatchEvent("eval.error", dataEvent);
            });
    };

    /**
     * Restart the kernel.
     */
    that.restart = function () {
        if( !that.loaded ) { return ; }
        return pyodide.globals.basthon.kernel.restart();
    };

    /**
     * Put a file on the local (emulated) filesystem.
     */
    that.putFile = function (filename, content) {
        pyodide.globals.basthon.kernel.put_file(filename, content);
    };

    /**
     * Put an importable module on the local (emulated) filesystem
     * and load dependencies.
     */
    that.putModule = function (filename, content) {
        return pyodide.globals.basthon.kernel.put_module(filename, content);
    };

    /**
     * Put a ressource (file or *.py module) on the local (emulated)
     * filesystem. Detection is based on extension.
     */
    that.putRessource = function (filename, content) {
        const ext = filename.split('.').pop();
        if( ext === 'py' ) {
            return that.putModule(filename, content);
        } else {
            return that.putFile(filename, content);
        }
    };

    /**
     * Is the source ready to be evaluated or want we more?
     * Usefull to set ps1/ps2 for teminal prompt.
     */
    that.more = function (source) {
        return pyodide.globals.basthon.kernel.more(source);
    };

    /**
     * Mimic the CPython's REPL banner.
     */
    that.banner = function () {
        return pyodide.globals.basthon.kernel.banner();
    };

    /**
     * Complete a code at the end (usefull for tab completion).
     *
     * Returns an array of two elements: the list of completions
     * and the start index.
     */
    that.complete = function (code) {
        return pyodide.globals.basthon.kernel.complete(code);
    }

    return that;
}));
