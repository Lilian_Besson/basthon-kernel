"""
Partial IPython implementation to access usefull methods in Basthon.
"""


from . import display


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ['display']
