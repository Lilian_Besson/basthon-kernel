import pkg_resources
import js


# loading proj4js
with open(pkg_resources.resource_filename(
            'proj4py', 'proj4.min.js')) as f:
    jscode = f.read()

js.eval(jscode)

# internal proj4 is main JS proj4
proj4 = js.proj4

# Lambert 93 definition
proj4.defs("EPSG:2154",
           "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs")
