def test_completion(selenium):
    assert selenium.run_js("return Basthon.complete('print.__d')") == [[
        'print.__delattr__(',
        'print.__dir__(',
        'print.__doc__'], 0]
