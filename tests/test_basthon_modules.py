from pathlib import Path
import base64
import time

_test_data = Path(__file__).parent / "data"


def test_all(selenium):
    # ensure all basthon modules are tested
    tested = set(g[len('test_'):] for g in globals()
                 if g.startswith('test_') and g != 'test_all')
    result = selenium.run_basthon("""
    from basthon import packages
    set(packages._internal_pkgs.keys())""")['result']
    internal = eval(result['result']['text/plain'])
    assert tested == internal


def test_turtle(selenium):
    # already tested in test_hacked_modules
    assert True


def test_folium(selenium):
    # already tested in test_hacked_modules
    assert True


def test_graphviz(selenium):
    result = selenium.run_basthon("""
    from graphviz import Digraph
    import basthon

    g = Digraph('G')
    g.edge('Hello', 'World')
    basthon.display(g)
    print(g.source)
    """)
    assert result['stderr'] == ""
    assert result['stdout'] == "digraph G {\n\tHello -> World\n}\n"
    assert result['display']['display_type'] == 'multiple'
    svg = result['display']['content']['image/svg+xml']
    with open(_test_data / "graphviz.svg") as f:
        target = f.read()
    assert target == svg


def test_requests(selenium):
    result = selenium.run_basthon("""
    import requests

    url = "http://httpbin.org/html"
    response = requests.get(url)
    print(response.text)
    response.headers
    """)
    assert result['stderr'] == ""
    with open(_test_data / "requests.txt") as f:
        target = f.read()
    assert target == result['stdout']
    headers = eval(result['result']['result']['text/plain'])
    assert headers['content-type'] == 'text/html; charset=utf-8'


def test_proj4py(selenium):
    result = selenium.run_basthon("""
    import proj4py

    # from WGS84 to Lambert93
    proj = proj4py.proj4('EPSG:4326', 'EPSG:2154')
    proj.forward((46.57824382381372, 2.468613626422624))
    """)
    assert result['stderr'] == ""
    assert result['stdout'] == ""
    target = eval(result['result']['result']['text/plain'])
    assert target == (659306.8946611215, 6608826.400123728)


def test_IPython(selenium):
    result = selenium.run_basthon("""
    import IPython
    import IPython.display
    from IPython.display import display, display_image
    """)
    assert result['stderr'] == ""
    assert result['stdout'] == ""


def test_p5(selenium):
    selenium.run_basthon("""
    from p5 import *

    x = 100
    y = 100

    def setup():
        createCanvas(200, 200)

    def draw():
        background(0)
        fill(255)
        rect(x, y, 50, 50)

    run()
    """, return_data=False)
    basename = f"p5_{selenium.driver.capabilities['browserName']}"
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    html = selenium.run_js("return window._basthon_eval_data.display.content.outerHTML")
    with open(_test_data / f"{basename}.html") as f:
        target = f.read()
    assert html == target
    selenium.run_js("document.body.appendChild(window._basthon_eval_data.display.content);")
    # this should be replaced with a clean selenium wait
    time.sleep(1)
    png = selenium.run_js("""
    const elem = window._basthon_eval_data.display.content;
    return elem.getElementsByTagName('canvas')[0].toDataURL('image/png');
    """)
    png = base64.b64decode(png[len('data:image/png;base64,'):])
    with open(_test_data / f"{basename}.png", 'rb') as f:
        target = f.read()
    assert png == target

    selenium.run_basthon("stop()")
